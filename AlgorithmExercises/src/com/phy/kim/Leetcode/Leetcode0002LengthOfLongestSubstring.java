package com.phy.kim.Leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author RiceDoggyRoll
 * 滑动窗口！想象一个窗口对着有序列表进行右移动和拓展！
 * 1 3 6 11 15
 */
public class Leetcode0002LengthOfLongestSubstring {

  public static void main(String[] args) {
    String s = "abcabcbb";
    System.out.println(new Leetcode0002LengthOfLongestSubstring().lengthOfLongestSubstring(s));
  }

  public int lengthOfLongestSubstring(String s) {
    if (s == null) {
      return 0;
    }
    Set<Character> occ = new HashSet<Character>();
    int rightBound = -1;
    int res=0;
    for (int i = 0; i < s.length(); i++) {
      if (i != 0) {
        occ.remove(s.charAt(i - 1));
      }
      while (rightBound + 1<s.length()&&!occ.contains(s.charAt(rightBound+1))){
        occ.add(s.charAt(rightBound+1));
        rightBound++;
      }
      res = Math.max(res,rightBound+1-i);
    }
    return res;
  }
}
