package com.phy.kim.foroffer;

/**
 * @author RiceDoggyRoll
 * 给定一个字符串数组 words，请计算当两个字符串 words[i] 和 words[j] 不包含相同字符时，
 * 它们长度的乘积的最大值。假设字符串中只包含英语的小写字母。
 * 如果没有不包含相同字符的一对字符串，返回 0。
 *
 */
public class Offer005MaxProduct {

  public int maxProduct(String[] words) {
    int[] nums = new int[words.length];
    int index = 0;
    for (String str : words) {
      nums[index++]= getCharNum(str);
    }
    int ans = 0;
    for (int i = 0; i < words.length; i++) {
      for (int j = 0; j < i; j++) {
        if ((nums[i] & nums[j]) == 0) {
          ans = Math.max(ans, words[i].length() * words[j].length());
        }
      }
    }
    return ans;
  }

  int getCharNum(String str) {
    int num = 0;
    for (int i = 0; i < str.length(); i++) {
      char letter = str.charAt(i);
      int offset = letter - 'a';
      num |= (1 << offset);
    }
    return num;
  }
}
