package com.phy.kim.foroffer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: RiceDoggyRoll
 * @Date: 2021/11/21 11:46
 * @Description:给定一个包含 n 个整数的数组nums， 判断nums中是否存在三个元素a ，b ，c ，使得a + b + c = 0 ？请找出所有和为 0 且不重复的三元组。
 */
public class Offer007ThreeSum {

  public static void main(String[] args) {
    int[] nums = {-1, 0, 1, 2, -1, -4};
    System.out.println(new Offer007ThreeSum().threeSum1(nums).toString());
  }

    public List<List<Integer>> threeSum1(int[] nums) {
      Arrays.sort(nums);
      List<List<Integer>> ans = new ArrayList<List<Integer>>();
      //第一个数
      for (int a = 0; a < nums.length; a++) {
        if (a > 0 && nums[a] == nums[a - 1]) {
          // 需要和上一次枚举的数不相同
          continue;
        }
        // 右指针
        int c = nums.length - 1;
        int target = -nums[a];
        //左指针
        for (int b = a + 1; b < nums.length; b++) {
          if (b > a + 1 && nums[b] == nums[b - 1]) {
            // 需要和上一次枚举的数不相同
            continue;
          }
          while (b < c && (nums[b] + nums[c]) > target) {
            // 如果指针重合，随着 b 后续的增加
            // 就不会有满足 a+b+c=0 并且 b<c 的 c 了，可以退出循环
            c--;
          }
          if (b == c) {
            break;
          }
          if (nums[b] + nums[c] == target) {
            ArrayList<Integer> tempList = new ArrayList<>();
            tempList.add(nums[a]);
            tempList.add(nums[b]);
            tempList.add(nums[c]);
            ans.add(tempList);
          }

        }
      }
      return ans;
  }

  public List<List<Integer>> threeSum(int[] nums) {
    int n = nums.length;
    Arrays.sort(nums);
    List<List<Integer>> ans = new ArrayList<List<Integer>>();
    // 枚举 a
    for (int first = 0; first < n; ++first) {
      // 需要和上一次枚举的数不相同
      if (first > 0 && nums[first] == nums[first - 1]) {
        continue;
      }
      // c 对应的指针初始指向数组的最右端
      int third = n - 1;
      int target = -nums[first];
      // 枚举 b
      for (int second = first + 1; second < n; ++second) {
        // 需要和上一次枚举的数不相同
        if (second > first + 1 && nums[second] == nums[second - 1]) {
          continue;
        }
        // 需要保证 b 的指针在 c 的指针的左侧
        while (second < third && nums[second] + nums[third] > target) {
          --third;
        }
        // 如果指针重合，随着 b 后续的增加
        // 就不会有满足 a+b+c=0 并且 b<c 的 c 了，可以退出循环
        if (second == third) {
          break;
        }
        if (nums[second] + nums[third] == target) {
          List<Integer> list = new ArrayList<Integer>();
          list.add(nums[first]);
          list.add(nums[second]);
          list.add(nums[third]);
          ans.add(list);
        }
      }
    }
    return ans;
  }

}
