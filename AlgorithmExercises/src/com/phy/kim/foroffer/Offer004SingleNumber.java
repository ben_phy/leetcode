package com.phy.kim.foroffer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author RiceDoggyRoll
 * 给你一个整数数组 nums ，除某个元素仅出现 一次 外，其余每个元素都恰出现 三次 。
 * 请你找出并返回那个只出现了一次的元素。
 */
public class Offer004SingleNumber {

  public static void main(String[] args) {
    System.out.println(2 & 2);
  }

  public int singleNumber1(int[] nums) {
    Map<Integer, Integer> freqMap = new HashMap<Integer, Integer>();
    for (int num : nums) {
      freqMap.put(num, freqMap.getOrDefault(num, 0) + 1);
    }
    int res = 0;
    for (Map.Entry<Integer, Integer> entry : freqMap.entrySet()) {
      int num = entry.getKey(), occ = entry.getValue();
      if (occ == 1) {
        res = num;
        break;
      }
    }
    return res;
  }

  public int singleNumber(int[] nums) {
    int res = 0;
    for (int i = 0; i < 32; i++) {
      //每位数加起来对三取余
      int sum = 0;
      for (int num : nums){
        sum += (num >> i) & 1;
      }
      sum %= 3;
      res += sum << i;
    }
    return res;
  }

}
