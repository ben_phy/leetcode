package com.phy.kim.foroffer;

/**
 * @Author: RiceDoggyRoll
 * @Date: 2021/11/22 0:05
 * @Description:
 * 给定一个正整数数组 nums和整数 k ，请找出该数组内乘积小于 k 的连续的子数组的个数。
 */
public class Offer009NumSubarrayProductLessThanK {
  public int numSubarrayProductLessThanK(int[] nums, int k) {
    int n = nums.length;
    int left = 0;
    int product = 1;
    int ans = 0;
    for (int right = 0; right < n; right++){
      product *= nums[right];
      while (left <= right && product >=k){
        product /=nums[left++];
      }
       ans +=left<=right?right-left +1:0;
    }
    return ans;
  }
}
