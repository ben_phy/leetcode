package basic_class_01;

import java.util.HashMap;

class Solution {

  public static void main(String[] args) {
//    System.out.println(new Solution().toHex(-26));
    System.out.println(new Solution().CheckPermutation("a", "ab"));
  }

  public String toHex(int num) {
    if (num == 0) {
      return "0";
    }
    StringBuilder sb = new StringBuilder();
    while (num != 0) {
      int u = num & 0xf;
      char c = (char) (u + '0');
      if (u >= 10) {
        c = (char) (u - 10 + 'a');
      }
      sb.append(c);
      num >>>= 4;
    }
    return sb.reverse().toString();
  }


  public boolean CheckPermutation(String s1, String s2) {
    HashMap<Character, Integer> map1 = new HashMap<>(100);
    HashMap<Character, Integer> map2 = new HashMap<>(100);
    for (int i = 0; i < s1.length(); i++) {
      char letter = s1.charAt(i);
      if (map1.containsKey(letter)) {
        map1.put(letter, map1.get(letter) + 1);
      } else {
        map1.put(letter, 1);
      }
    }
    for (int i = 0; i < s2.length(); i++) {
      char letter = s2.charAt(i);
      if (map2.containsKey(letter)) {
        map2.put(letter, map2.get(letter) + 1);
      } else {
        map2.put(letter, 1);
      }
    }
    if (map1.size() != map2.size()) {
      return false;
    }
    for (Character key : map1.keySet()) {
      if (!map1.get(key).equals(map2.get(key))) {
        return false;
      }
    }
    return true;
  }
}